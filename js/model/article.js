App.Article = DS.Model.extend({
  title: DS.attr('string'),
  author: DS.attr('string'),
  isPublished: DS.attr('boolean')
});


App.Article.FIXTURES = [{
   id:1,
   title: 'Playing with Java 8 Lambda Expressions',
   author: 'Micha Kops',
   isPublished: true
},{
   id:2,
   title: 'Java EE 7 Batch Processing',
   author: 'Micha Kops',
   isPublished: false
},{
   id:3,
   title: 'ORM is teh evil',
   author: 'Bobert R. Cartin',
   isPublished: false
}];

