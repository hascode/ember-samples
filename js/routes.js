App.Router.map(function () {
  this.resource('index', { path: '/' });
  this.resource('articles', { path: '/articles' });
});

App.ArticlesRoute = Ember.Route.extend({
  model: function () {
    return this.store.find('article');
  }
});
